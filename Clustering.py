import numpy as np
from numpy import random
from time import time
from collections import defaultdict

from recherche import Recherche
from entrainementBD import EntrainementBD


def ls(u, v):
    return np.sum((u-v)**2)

# les coordonnées c'est le calcul de sqrt(least_squares) + n élément // n élément
class Clustering():
    SEED = 42

    def __init__(self, nbResultats, nbCentroides, dico, matrice):
        np.random.seed(self.SEED)
        self.nbMots = len(dico)
        self.dico = {v: k for k, v in dico.items()}
        self.mots_par_cluster = defaultdict(lambda: [])
        self.matrice = matrice
        self.nbCentroides = nbCentroides
        self.futureCentroid = []
        self.nbResultats = nbResultats

    def clustering(self):
        centroids = self._find_initial_centroid()
        i = 0
        v_app = self._next_clusters(centroids)

        changements = self.nbMots

        while(changements > 0):
            t = time()
            centroids, compteur = self._find_centroids(v_app)
            v_app_new = self._next_clusters(centroids)
            changements = np.sum((np.not_equal(v_app, v_app_new)))

            self._displayResults(changements, compteur, i, t)
            v_app = v_app_new
            i += 1

        self._results()

    def _find_centroids(self, v_app):
        centroids = np.zeros((self.nbCentroides, self.nbMots))
        compteur = np.zeros(self.nbCentroides)

        for i in range(self.nbMots):
            centroids[v_app[i]] += self.matrice[i]
            compteur[v_app[i]] += 1

        for i in range(self.nbCentroides):
            if compteur[i] > 0:
                centroids[i] /= compteur[i]

        return centroids, compteur

    def _find_initial_centroid(self):
        """ Retourne un index aléatoire au minimum de 0 et maximum du nombre de mot dans la base de donnée """
        randIndex = np.random.choice(self.nbMots, self.nbCentroides, replace=False)
        centroids = np.zeros((self.nbCentroides, self.nbMots))
        for i in range(self.nbCentroides):
            centroids[i] = self.matrice[randIndex[i]]
        return centroids

    def _next_clusters(self, centroids):
        v_app = np.zeros(self.nbMots, dtype=int)
        self.mots_par_cluster.clear()

        for i in range(self.nbMots):
            scores = [ls(self.matrice[i], c) for c in centroids]
            val_min = min(scores)
            v_app[i] = scores.index(val_min)
            self.mots_par_cluster[v_app[i]].append((self.dico[i], val_min))

        return v_app

    def _displayResults(self, changements, compteur, i, t):
        print("========================================================")
        print("Iteration " + str(i))
        print("{n} changements de clusters en {s} secondes.".format(n=str(changements), s=str(time() - t)))
        j = 0
        totalMots = 0
        for c in compteur:
            # nbMots = np.count_nonzero(c)
            nbMots = c
            print("Il y a {nb} points (mots) regroupés autour du centroïde no {numC}.".format(nb=nbMots, numC=j))
            totalMots += nbMots
            j += 1
        print("")
        print(totalMots)
        print("========================================================")

    def _results(self):
        for cluster_idx, liste_mot_distance in sorted(self.mots_par_cluster.items()):
            print("\n")
            i = 0
            liste_mot_distance.sort(key=lambda tup: tup[1])
            print("Cluster {}".format(cluster_idx))

            for mot, distance in liste_mot_distance:
                if(i == self.nbResultats):
                    break

                print("{} ----> {}".format(mot, distance))
                i += 1

