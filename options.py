from sys import argv, exit
import argparse

class Options:
    pass

def creer_parseur():
    p = argparse.ArgumentParser()
    p.add_argument('-c', action="store_true", dest="clustering", help="Active le clustering")
    p.add_argument('-e', action='store_true', dest='entrainement', help="Execute l'entrainement")
    p.add_argument('-r', action='store_true', dest='recherche', help="Execute la recherche")
    p.add_argument('-b', action='store_true', dest='bd', help="Crée la base de donnée si celle-ci n'exite pas")
    p.add_argument('-t', dest='fenetre', type=int, help="Prend la taille de la fenêtre(impair)")
    p.add_argument('-n', action="store", type=int, dest="nombre_resultats_cluster", help="Nombre de mots par clusters")
    p.add_argument('-k', action="store", type=int, dest="nombre_centroides", help="Nombre de centroides")
    p.add_argument('--enc', dest='encodage', type=str, default="utf-8", help="Encodage ex.: utf-8")
    p.add_argument('--chemin', dest='chemin', type=str, nargs="+", help="Chemin(s) des textes")
    p.add_argument('--dbs', dest='databases_path', type=int, help="Choix de la base de données (1: Poemes, 2: Chroniques, 3: Histoires pour enfants")

    return p

def options(arguments):
    opt = Options()
    p = creer_parseur()
    p.parse_args(args=arguments, namespace=opt)

    if len(argv) == 1:
        p.print_help()
    else:
        if opt.entrainement + opt.recherche + opt.bd + opt.clustering != 1:
            raise Exception('Vous devez choisir une seule option entre entraînement, cluster et recherche.')

        if opt.bd:
            return opt

        if opt.fenetre is None or opt.fenetre < 3 or opt.fenetre % 2 == 0:
            raise Exception('La taille de la fenêtre doit être un nombre impair supérieur à 1.')

        if opt.entrainement:
            if opt.chemin == None or opt.encodage == None:
                raise Exception('Pour l\'entraînement, vous devez fournir un chemin et un encodage.')
            for fichier in opt.chemin:
                with open(fichier, 'r', encoding = opt.encodage) as f:
                    pass

    return opt
