import numpy as np
import re

EXPRESSION_SEPARATEUR = '\w+'
EXPRESSION_FILTRE = '[\-«»\'\’(),.:;!?\\_\n\d"]'


# ceci devrait se nommer le Modele...
class Entrainement():
    def __init__(self, fenetre):
        self.fenetre = fenetre

    def entrainer(self, chemin, encodage):
        self.texte = self.parser(chemin, encodage)
        self._dict_vocabulaire = self.extraire_vocabulaire()
        self.texte = [self._dict_vocabulaire[mot] for mot in self.texte]
        self._dict_cooccurrences = self.extraire_cooccurrences()
        self._matrice = self.remplir_matrice(self._dict_vocabulaire, self._dict_cooccurrences)

    def parser(self, chemins, encodage):
        liste_mots_bruts = []
        for chemin in chemins:
            with open(chemin, 'r', encoding=encodage) as fichier:
                temp = fichier.read()
                temp = re.sub(EXPRESSION_FILTRE, ' ', temp)
                liste_mots_bruts += re.findall(EXPRESSION_SEPARATEUR, temp.lower())
        return liste_mots_bruts

    def extraire_vocabulaire(self):
        dict_vocabulaire = {}
        for mot in self.texte:
            if mot not in dict_vocabulaire:
                dict_vocabulaire[mot] = len(dict_vocabulaire)
        return dict_vocabulaire

    # les mots sont des indexes
    def extraire_cooccurrences(self):
        dict_cooccurrences = {}
        taille_fenetre = self.fenetre // 2
        offsets = list(range(1, taille_fenetre + 1))

        for i in range(taille_fenetre):
            ir = self.texte[i]
            for j in offsets:
                if i-j < 0:
                    break
                ic = self.texte[i-j]
                Entrainement._analyse_coocurence(ir, ic, dict_cooccurrences)

        for i in range(taille_fenetre, len(self.texte)):
            ir = self.texte[i]
            for j in offsets:
                ic = self.texte[i-j]
                Entrainement._analyse_coocurence(ir, ic, dict_cooccurrences)
        return dict_cooccurrences

    @staticmethod
    def _analyse_coocurence(ir, ic, dict_cooccurrences):
        if ir <= ic:
            coocc = (ir, ic)
            if coocc not in dict_cooccurrences:
                dict_cooccurrences[coocc] = 1
            else:
                dict_cooccurrences[coocc] += 1
        if ic <= ir:
            coocc = (ic, ir)
            if coocc not in dict_cooccurrences:
                dict_cooccurrences[coocc] = 1
            else:
                dict_cooccurrences[coocc] += 1

    def remplir_matrice(self, dict_mots_uniques, dict_cooccurrences):
        matrice = np.zeros((len(dict_mots_uniques), len(dict_mots_uniques)))
        for (ir, ic), freq in dict_cooccurrences.items():
            matrice[ir][ic] = freq
            matrice[ic][ir] = freq
        return matrice
