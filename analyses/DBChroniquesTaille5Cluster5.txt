========================================================
Iteration 0
969 changements de clusters en 0.9805788993835449 secondes.
Il y a 379.0 points (mots) regroupés autour du centroïde no 0.
Il y a 8217.0 points (mots) regroupés autour du centroïde no 1.
Il y a 906.0 points (mots) regroupés autour du centroïde no 2.
Il y a 388.0 points (mots) regroupés autour du centroïde no 3.
Il y a 16.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 1
666 changements de clusters en 0.9727756977081299 secondes.
Il y a 411.0 points (mots) regroupés autour du centroïde no 0.
Il y a 8240.0 points (mots) regroupés autour du centroïde no 1.
Il y a 1066.0 points (mots) regroupés autour du centroïde no 2.
Il y a 127.0 points (mots) regroupés autour du centroïde no 3.
Il y a 62.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 2
928 changements de clusters en 0.9694414138793945 secondes.
Il y a 519.0 points (mots) regroupés autour du centroïde no 0.
Il y a 8187.0 points (mots) regroupés autour du centroïde no 1.
Il y a 1074.0 points (mots) regroupés autour du centroïde no 2.
Il y a 51.0 points (mots) regroupés autour du centroïde no 3.
Il y a 75.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 3
939 changements de clusters en 1.00801420211792 secondes.
Il y a 386.0 points (mots) regroupés autour du centroïde no 0.
Il y a 8028.0 points (mots) regroupés autour du centroïde no 1.
Il y a 1397.0 points (mots) regroupés autour du centroïde no 2.
Il y a 32.0 points (mots) regroupés autour du centroïde no 3.
Il y a 63.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 4
499 changements de clusters en 1.020029067993164 secondes.
Il y a 253.0 points (mots) regroupés autour du centroïde no 0.
Il y a 8036.0 points (mots) regroupés autour du centroïde no 1.
Il y a 1547.0 points (mots) regroupés autour du centroïde no 2.
Il y a 23.0 points (mots) regroupés autour du centroïde no 3.
Il y a 47.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 5
353 changements de clusters en 1.007246494293213 secondes.
Il y a 139.0 points (mots) regroupés autour du centroïde no 0.
Il y a 8291.0 points (mots) regroupés autour du centroïde no 1.
Il y a 1407.0 points (mots) regroupés autour du centroïde no 2.
Il y a 18.0 points (mots) regroupés autour du centroïde no 3.
Il y a 51.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 6
216 changements de clusters en 1.0167315006256104 secondes.
Il y a 146.0 points (mots) regroupés autour du centroïde no 0.
Il y a 8610.0 points (mots) regroupés autour du centroïde no 1.
Il y a 1099.0 points (mots) regroupés autour du centroïde no 2.
Il y a 13.0 points (mots) regroupés autour du centroïde no 3.
Il y a 38.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 7
154 changements de clusters en 1.0159809589385986 secondes.
Il y a 142.0 points (mots) regroupés autour du centroïde no 0.
Il y a 8792.0 points (mots) regroupés autour du centroïde no 1.
Il y a 935.0 points (mots) regroupés autour du centroïde no 2.
Il y a 11.0 points (mots) regroupés autour du centroïde no 3.
Il y a 26.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 8
136 changements de clusters en 1.0589661598205566 secondes.
Il y a 123.0 points (mots) regroupés autour du centroïde no 0.
Il y a 8916.0 points (mots) regroupés autour du centroïde no 1.
Il y a 834.0 points (mots) regroupés autour du centroïde no 2.
Il y a 8.0 points (mots) regroupés autour du centroïde no 3.
Il y a 25.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 9
124 changements de clusters en 1.0118129253387451 secondes.
Il y a 104.0 points (mots) regroupés autour du centroïde no 0.
Il y a 9027.0 points (mots) regroupés autour du centroïde no 1.
Il y a 745.0 points (mots) regroupés autour du centroïde no 2.
Il y a 8.0 points (mots) regroupés autour du centroïde no 3.
Il y a 22.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 10
101 changements de clusters en 1.016603946685791 secondes.
Il y a 82.0 points (mots) regroupés autour du centroïde no 0.
Il y a 9124.0 points (mots) regroupés autour du centroïde no 1.
Il y a 672.0 points (mots) regroupés autour du centroïde no 2.
Il y a 7.0 points (mots) regroupés autour du centroïde no 3.
Il y a 21.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 11
81 changements de clusters en 1.0057718753814697 secondes.
Il y a 64.0 points (mots) regroupés autour du centroïde no 0.
Il y a 9207.0 points (mots) regroupés autour du centroïde no 1.
Il y a 607.0 points (mots) regroupés autour du centroïde no 2.
Il y a 7.0 points (mots) regroupés autour du centroïde no 3.
Il y a 21.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 12
54 changements de clusters en 1.0548958778381348 secondes.
Il y a 56.0 points (mots) regroupés autour du centroïde no 0.
Il y a 9280.0 points (mots) regroupés autour du centroïde no 1.
Il y a 542.0 points (mots) regroupés autour du centroïde no 2.
Il y a 7.0 points (mots) regroupés autour du centroïde no 3.
Il y a 21.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 13
40 changements de clusters en 1.0850024223327637 secondes.
Il y a 53.0 points (mots) regroupés autour du centroïde no 0.
Il y a 9331.0 points (mots) regroupés autour du centroïde no 1.
Il y a 494.0 points (mots) regroupés autour du centroïde no 2.
Il y a 7.0 points (mots) regroupés autour du centroïde no 3.
Il y a 21.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 14
39 changements de clusters en 1.0094397068023682 secondes.
Il y a 51.0 points (mots) regroupés autour du centroïde no 0.
Il y a 9369.0 points (mots) regroupés autour du centroïde no 1.
Il y a 458.0 points (mots) regroupés autour du centroïde no 2.
Il y a 7.0 points (mots) regroupés autour du centroïde no 3.
Il y a 21.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 15
29 changements de clusters en 1.0525610446929932 secondes.
Il y a 49.0 points (mots) regroupés autour du centroïde no 0.
Il y a 9406.0 points (mots) regroupés autour du centroïde no 1.
Il y a 423.0 points (mots) regroupés autour du centroïde no 2.
Il y a 7.0 points (mots) regroupés autour du centroïde no 3.
Il y a 21.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 16
16 changements de clusters en 1.0175068378448486 secondes.
Il y a 47.0 points (mots) regroupés autour du centroïde no 0.
Il y a 9433.0 points (mots) regroupés autour du centroïde no 1.
Il y a 398.0 points (mots) regroupés autour du centroïde no 2.
Il y a 7.0 points (mots) regroupés autour du centroïde no 3.
Il y a 21.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 17
16 changements de clusters en 1.043534278869629 secondes.
Il y a 43.0 points (mots) regroupés autour du centroïde no 0.
Il y a 9445.0 points (mots) regroupés autour du centroïde no 1.
Il y a 390.0 points (mots) regroupés autour du centroïde no 2.
Il y a 7.0 points (mots) regroupés autour du centroïde no 3.
Il y a 21.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 18
18 changements de clusters en 0.9729559421539307 secondes.
Il y a 42.0 points (mots) regroupés autour du centroïde no 0.
Il y a 9458.0 points (mots) regroupés autour du centroïde no 1.
Il y a 379.0 points (mots) regroupés autour du centroïde no 2.
Il y a 7.0 points (mots) regroupés autour du centroïde no 3.
Il y a 20.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 19
10 changements de clusters en 0.9634718894958496 secondes.
Il y a 40.0 points (mots) regroupés autour du centroïde no 0.
Il y a 9474.0 points (mots) regroupés autour du centroïde no 1.
Il y a 365.0 points (mots) regroupés autour du centroïde no 2.
Il y a 7.0 points (mots) regroupés autour du centroïde no 3.
Il y a 20.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 20
10 changements de clusters en 0.9581606388092041 secondes.
Il y a 38.0 points (mots) regroupés autour du centroïde no 0.
Il y a 9482.0 points (mots) regroupés autour du centroïde no 1.
Il y a 359.0 points (mots) regroupés autour du centroïde no 2.
Il y a 7.0 points (mots) regroupés autour du centroïde no 3.
Il y a 20.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 21
5 changements de clusters en 0.9527809619903564 secondes.
Il y a 37.0 points (mots) regroupés autour du centroïde no 0.
Il y a 9491.0 points (mots) regroupés autour du centroïde no 1.
Il y a 351.0 points (mots) regroupés autour du centroïde no 2.
Il y a 7.0 points (mots) regroupés autour du centroïde no 3.
Il y a 20.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 22
1 changements de clusters en 0.9721822738647461 secondes.
Il y a 37.0 points (mots) regroupés autour du centroïde no 0.
Il y a 9496.0 points (mots) regroupés autour du centroïde no 1.
Il y a 346.0 points (mots) regroupés autour du centroïde no 2.
Il y a 7.0 points (mots) regroupés autour du centroïde no 3.
Il y a 20.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 23
2 changements de clusters en 0.9854333400726318 secondes.
Il y a 37.0 points (mots) regroupés autour du centroïde no 0.
Il y a 9497.0 points (mots) regroupés autour du centroïde no 1.
Il y a 345.0 points (mots) regroupés autour du centroïde no 2.
Il y a 7.0 points (mots) regroupés autour du centroïde no 3.
Il y a 20.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 24
1 changements de clusters en 0.9807772636413574 secondes.
Il y a 37.0 points (mots) regroupés autour du centroïde no 0.
Il y a 9499.0 points (mots) regroupés autour du centroïde no 1.
Il y a 343.0 points (mots) regroupés autour du centroïde no 2.
Il y a 7.0 points (mots) regroupés autour du centroïde no 3.
Il y a 20.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================
========================================================
Iteration 25
0 changements de clusters en 0.9945425987243652 secondes.
Il y a 37.0 points (mots) regroupés autour du centroïde no 0.
Il y a 9500.0 points (mots) regroupés autour du centroïde no 1.
Il y a 342.0 points (mots) regroupés autour du centroïde no 2.
Il y a 7.0 points (mots) regroupés autour du centroïde no 3.
Il y a 20.0 points (mots) regroupés autour du centroïde no 4.

9906.0
========================================================


Cluster 0
faire ----> 1436.7143900657416
temps ----> 1470.4441197954711
bien ----> 1544.6603360116874
même ----> 1688.6062819576337
vie ----> 1748.065741417093
fait ----> 1748.876552227904
sa ----> 1776.7684441197955
aux ----> 1800.4441197954711
nos ----> 1887.2549306062824
cette ----> 1986.8224981738497


Cluster 1
coût ----> 2.399562171745152
perpétuation ----> 2.404825329639889
accroissement ----> 2.404825329639889
lourdeur ----> 2.404825329639889
tabou ----> 2.5305095401662046
promulgation ----> 2.5496674349030464
maternelle ----> 2.557456908587257
ghettos ----> 2.5580884875346257
entrailles ----> 2.580825329639889
inspecteur ----> 2.629877961218836


Cluster 2
sécurité ----> 86.96663075818202
vient ----> 88.1479173078896
ensemble ----> 92.45785882835744
base ----> 96.14791730788959
pratique ----> 96.15961321432236
réalité ----> 96.75610444239251
membres ----> 97.22978865291884
passer ----> 99.69177695701241
partage ----> 100.84967169385452
manque ----> 101.45201087514106


Cluster 3
et ----> 46013.24489795919
le ----> 72981.24489795917
à ----> 82028.95918367349
les ----> 90273.8163265306
l ----> 104554.38775510204
la ----> 240732.10204081633
de ----> 613980.5306122451


Cluster 4
qui ----> 15500.15
pour ----> 16030.25
en ----> 18577.850000000002
du ----> 18840.850000000002
ce ----> 21928.749999999996
on ----> 23278.75
dans ----> 28965.149999999998
que ----> 32628.45
qu ----> 33825.950000000004
une ----> 35862.95
