import numpy as np

def mots_stops():
        with open('stopList.txt', 'r', encoding='UTF8') as fichier:
            return fichier.read().split('\n')
      
def least_squares(u, v):
    return np.sum((u-v)**2)

def city_block(u, v):
    return np.sum(np.abs(u-v))

class Recherche():
    def __init__(self, d, m):
        self.d, self.m = d, m
        self.stop = mots_stops
        

    def chercher(self, mot, nb, methode):
        if mot not in self.d:
            print('"{}" inconnu'.format(mot))
            return

        if methode == 0:
            formule = np.dot
        elif methode == 1:
            formule = least_squares
        elif methode == 2:
            formule = city_block
        else:
            print('Méthode inconnue')
            return

        for score, mot in self.calculer(mot, nb, formule):
             print(mot, '-->', score)

    def calculer(self, mot_r, nb, f): # mot_r est le mot recherché
        i_r = self.d[mot_r] # On sort l'index du mot recherché dans dictionnaire
        scores = []
        for mot, i in self.d.items(): # pour les clefs et valeurs dans self.d
            #if i != i_r and mot not in self.stop: #si i != i_r et que le mot n'est pas dans la liste stop
            scores.append( (f(self.m[i_r], self.m[i]), mot) ) # calcule le least square et ajoute le résultat à la variable score
        return sorted(scores, reverse = f == np.dot)[:nb]
    

