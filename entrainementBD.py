from entrainement import Entrainement


class EntrainementBD(Entrainement):
    def __init__(self, fenetre, dao):
        super().__init__(fenetre)
        self.dao = dao

    def chercher_mots(self):
        return dict(self.dao.chercher_mots())

    def chercher_cooccurrences(self):
        d = {}
        for ir, ic, tfen, freq in self.dao.chercher_cooccurrences(self.fenetre):
            d[(ir, ic)] = freq
        return d

    def chargerBD(self):
        self.dict_bd = self.chercher_mots()
        self.dict_cooccurrences_bd = self.chercher_cooccurrences()

    def dico_matrice(self):
        matrice = super().remplir_matrice(self.dict_bd, self.dict_cooccurrences_bd)
        return self.dict_bd, matrice

    def maj_mots(self):
        nouveau_mots = []
        for mot in self.texte:
            if mot not in self.dict_bd:
                nouveau_mots.append((mot, len(self.dict_bd)))
                self.dict_bd[mot] = len(self.dict_bd)

        self.dao.inserer_mots(nouveau_mots)

    def maj_cooccurrences(self, dict_cooccurrences):
        insert, update = [], []
        for (ir, ic), freq in dict_cooccurrences.items():
            if (ir, ic) in self.dict_cooccurrences_bd:
                update.append((freq + self.dict_cooccurrences_bd[(ir, ic)], ir, ic, self.fenetre))
            else:
                insert.append((ir, ic, self.fenetre, freq))

        self.dao.inserer_cooccurrences(insert)
        self.dao.maj_cooccurrences(update)

    def entrainer(self, chemin, encodage):
        self.texte = self.parser(chemin, encodage)
        self.maj_mots()
        self.texte = [self.dict_bd[mot] for mot in self.texte]
        dict_cooccurrences = self.extraire_cooccurrences()
        self.maj_cooccurrences(dict_cooccurrences)
