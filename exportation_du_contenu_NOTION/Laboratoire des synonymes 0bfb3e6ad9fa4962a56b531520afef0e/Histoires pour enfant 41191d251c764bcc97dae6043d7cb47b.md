# Histoires pour enfant

## Introduction

L'enfance est synonyme d'innocence, de pureté. L'enfant a tout à apprendre et rien à cacher. Il est ébloui par les petites choses et ne remet jamais en question l'irréel. Il apprend en touchant, en jouant, en écoutant et se fait border par les histoires enchanteresses que lui raconte ses parents. En plus de porter une morale, ces histoires l'amène à s'évader et cela dans un langage aussi pur que son être. Pas de fla-flas, chaque mot est une racine de notre langue, chaque phrase est un fragment d'un langage plus recherché. C'est ainsi qu'analyser ce genre littéraire avec une analyse de grappe automatisée devient intéressante puisque qu'elle nous ramène à, ce que fût un jour, l'innocence. 

## Hypothèse

Dans la mesure où les histoires pour enfant ont très peu de descriptions, les grappes seront pures, divisées en catégories de mots simple : verbes, adjectif, nom et pronom.

## Présentation des données

Les enfants, contrairement aux adultes, formulent des phrases courtes. Moins accès sur la description, on peut rapidement constater qu'avec seulement 5 grappes, c'est bien les histoires qui détiennent les données les plus intéressantes. Par manque de variété de mots, il devient fréquent d'avoir plusieurs cooccurrences et cela peu importe la taille de fenêtre. C'est d'ailleurs pour cette raison qu'on constate un grand écart entre les trois genres littéraires : on peut même dire que la taille de fenêtre 3 n'est appropriée que pour un genre qui, tel les histoires pour enfants, formule de courtes phrases.

- `*(Graphique 001)*`

    [https://www.notion.vip/notion-chart/draw.html?config_documentId=1psLfV3vAoiEqAvpchrOy34zOySZfYfojpenYWxgFMq8&config_sheetName=Taille3%20Cluster5&config_dataRange=A1%3AD6&config_chartType=line&config_theme=lightMode&option_colors=%23AD1A72%2C%23E03E3E%2C%230B6E99](https://www.notion.vip/notion-chart/draw.html?config_documentId=1psLfV3vAoiEqAvpchrOy34zOySZfYfojpenYWxgFMq8&config_sheetName=Taille3%20Cluster5&config_dataRange=A1%3AD6&config_chartType=line&config_theme=lightMode&option_colors=%23AD1A72%2C%23E03E3E%2C%230B6E99)

    *001 - Taille de fenêtre 3 avec 5 cluster*

Dès 5 grappes, nous pouvons commencer à constater une certaine pureté dans la répartition des mots par regroupement.

- `*(Résultats textuels - 001)*`

    ```css
    Cluster 1/5 -- Déterminants, pronoms, verbes
    mais ----> 3977.92
    avait ----> 4856.83
    était ----> 5381.47
    a ----> 6752.01
    y ----> 7754.19
    en ----> 7888.65
    n ----> 8304.65
    qui ----> 9005.56
    s ----> 9429.10
    lui ----> 9699.10

    Cluster 2/5 -- Noms au féminin
    maison ----> 803.97
    forêt ----> 925.62
    tête ----> 970.44
    mer ----> 1018.33
    reine ----> 1050.33
    terre ----> 1093.03
    nuit ----> 1114.68
    porte ----> 1130.44
    vieille ----> 1438.21
    par ----> 1558.09

    Cluster 4/5 -- Déterminants et pronoms
    et ----> 33570.95
    l ----> 41176.38
    à ----> 41192.10
    le ----> 61133.81
    il ----> 89619.95
    de ----> 110104.67
    la ----> 136532.38
    ```

À 15 grappes, cette pureté semble de plus en plus évidente.

- `*(Résultats textuels - 002)*`

    ```css
    Cluster 1/15
    alors ----> 1747.60
    avec ----> 2481.60
    par ----> 2554.74
    on ----> 2700.31
    bien ----> 2748.03
    comme ----> 2946.74
    grand ----> 3465.60
    jour ----> 4810.17
    petit ----> 4864.45
    tout ----> 5458.31

    Cluster 4/15
    son ----> 9392.63
    sa ----> 10752.02
    s ----> 11166.94
    des ----> 11571.25
    lui ----> 11734.02
    ses ----> 12018.79
    en ----> 16582.94
    se ----> 17244.63
    une ----> 17739.86
    un ----> 31301.25

    Cluster 9/15
    à ----> 62917.02
    et ----> 96211.02
    le ----> 104158.02
    il ----> 123533.02
    de ----> 184142.02
    la ----> 258079.69

    Cluster 10/15
    pour ----> 4236.65
    sur ----> 6699.40
    du ----> 8074.90
    qui ----> 12900.15
    dit ----> 13730.40
    plus ----> 15792.90
    d ----> 22388.40
    dans ----> 22619.90

    Cluster 12/15
    reine ----> 568.57
    mer ----> 614.31
    porte ----> 622.97
    maison ----> 630.57
    tête ----> 641.91
    nuit ----> 643.51
    terre ----> 856.44
    forêt ----> 865.64
    vieille ----> 995.11
    femme ----> 1804.57

    Cluster 13
    veux ----> 456.84
    sais ----> 580.41
    peux ----> 761.84
    me ----> 910.98
    vais ----> 1382.84
    m ----> 1642.70
    te ----> 2178.56
    es ----> 2180.70
    t ----> 2272.27
    as ----> 2673.84

    Cluster 15
    mais ----> 5669.51
    était ----> 8554.80
    avait ----> 9020.37
    a ----> 12545.22
    y ----> 13254.94
    ce ----> 14746.08
    n ----> 19703.80
    que ----> 28115.37
    je ----> 30459.94
    ne ----> 36922.37

    ```

À 40 grappes, on arrive même à avoir des groupes contenant presque juste des verbes.

- `*(Résultats textuels - 003)*`

    ```css
    Cluster 6
    ressortait ----> 0.88
    finira ----> 0.88
    courtisée ----> 0.99
    engourdi ----> 0.99
    manœuvré ----> 0.99
    remplacée ----> 1.11
    tués ----> 1.11
    recueillis ----> 1.11
    foudroyé ----> 1.11
    dupée ----> 1.11

    Cluster 21
    enfonça ----> 8.96
    endormit ----> 10.88
    élança ----> 11.66
    envole ----> 12.44
    éveilla ----> 13.40
    endormir ----> 14.35
    éloigne ----> 14.62
    empressa ----> 15.31
    approcher ----> 15.48
    endormirent ----> 15.75

    Cluster 23
    tend ----> 7.15
    shoote ----> 9.50
    songeait ----> 10.24
    grandit ----> 11.26
    suspendit ----> 12.47
    déposa ----> 12.71
    touche ----> 13.15
    pleurait ----> 13.40
    compagnie ----> 13.65
    marchait ----> 13.71
    ```

Par contre, tout se corse lorsqu'on se rend à 100 regroupements où le lien entre les mots de chacun de ceux-ci devient de moins en moins évident. 

Avec une taille de fenêtre 5, on peut constater une hausse des données. Sans toutefois dire que cette hausse représente un maillon faible, on peut constater que l'hypothèse de départ, tient de moins en moins la route.

- `*(Graphique 002)*`

    [https://www.notion.vip/notion-chart/draw.html?config_documentId=18bulc_If4eY6UEBMZsqgStcFFpsC9U_hwLbLl58k4Xs&config_sheetName=Taille5-Clusters5&config_dataRange=A1%3AD6&config_chartType=line&config_theme=lightMode&option_colors=%23AD1A72%2C%23E03E3E%2C%230B6E99](https://www.notion.vip/notion-chart/draw.html?config_documentId=18bulc_If4eY6UEBMZsqgStcFFpsC9U_hwLbLl58k4Xs&config_sheetName=Taille5-Clusters5&config_dataRange=A1%3AD6&config_chartType=line&config_theme=lightMode&option_colors=%23AD1A72%2C%23E03E3E%2C%230B6E99)

    *002 - Taille de fenêtre 5 avec 5 cluster*

Où, initialement à 5 grappes, nous avions un semblant d'une certaine pureté dans la répartition des mots par regroupement, en augmentant la taille de fenêtre, cette pureté est largement moins flagrante. 

- `*(Résultats textuels - 004)*`

    ```css
    Cluster 1/5
    mais ----> 7421.661157024794
    lui ----> 9492.327823691458
    était ----> 10426.661157024793
    du ----> 12020.752066115701
    des ----> 12044.752066115701
    au ----> 12160.267217630853
    avait ----> 12726.267217630853
    s ----> 12776.32782369146
    dit ----> 13677.7217630854
    en ----> 13764.327823691461

    Cluster 3/5
    à ----> 62917.02777777777
    et ----> 96211.02777777777
    le ----> 104158.02777777778
    il ----> 123533.02777777778
    de ----> 184142.0277777778
    la ----> 258079.69444444438
    ```

À 15 grappes, on retrouve des similitudes avec les résultats précédents entourant les termes tels les pronoms et les déterminant et à 40 grappes, on retrouve également des groupes formés presque uniquement de verbes, mais en moins grande quantité. En fait, aillant plus de données à répartir, la liaison entre les termes de chaque regroupement est moins évidente ou semble inexistante. Par contre, en augmentant la cooccurrence de certain mot, tel que les négateurs *n'* et *ne*, on réduit leur score à un point d'égalité qui permet un regroupement composé uniquement de ce duo.

- `*(Résultats textuels - 005)*`

    ```css
    Cluster 15/40 /* Taille 5 */
    n ----> 5880.0
    ne ----> 5880.0

    **VS**

    Cluster 40/40 /* Taille 3 */
    s ----> 5517.49
    se ----> 6166.16
    n ----> 8808.16
    lui ----> 10022.83
    ne ----> 10688.49
    qu ----> 33199.83
    ```

Chose certaine, plus il y a de grappes pour diviser le contenu unique des histoires et plus les grappes sont balancés et cela peu importe la taille de fenêtre. Toutefois, il reste toujours un groupe qui excèdent largement la grandeur des autres. Tel les poèmes, ce groupe contient les mots les moins utilisés au travers des 82 histoires.

## Conclusion

L'hypothèse de départ est fausse. Même si les textes pour enfants ont des phrases plus courtes, même si le vocabulaire y est moins recherché et même si on ne s'attarde qu'à une taille de fenêtre basse tel que 3, il est impossible de ressortir une véritable pureté dans les regroupements. Il est vrai qu'on obtient des groupes intéressants, plus concrets, mais c'est plutôt l'explication logique de notre connaissance de la langue qui nous pousse à vouloir voir ces résultats. En réalité, les grappes sont tirées vers une popularité commune, par une encre commune, sans plus. Le positionnement des mots fait toute la différence, certes, et la constance de ces positionnements en fait une plus grande, mais le lien entre les mots n'est point grammatical.