# Laboratoire des synonymes

## Introduction

Le français est une langue indo-européenne de la famille des langues romanes. Elle est la deuxième langue la plus souvent enseignée en tant que langue étrangère à travers le monde. On estime à 300 millions le nombre de locuteurs francophones. Le français a su se démarquer à travers le temps et c'est pourquoi cette langue mérite d'être étudiée, analysée et comprise. Il est, d'ailleurs, intéressant d'en voir ses variantes avec quelque 59 000 mots recensés par le *Larousse* et 132 000 mots recensés par le *Littré*. C'est ainsi qu'une analyse automatisée sous forme de grappe pourrait permettre un regroupement riche, logique et très pertinent autant en matière textuelle que grammaticale.

## Hypothèse principale

Peu importe le genre littéraire, plus le nombre de grappes sera élevé, plus les regroupements de mots seront précis : on peut alors parler de groupes nominaux, groupes adjectivaux, groupes verbaux ainsi que leurs expansions. 

## Analyse

Pour démontrer notre hypothèse, nous avons subdivisé notre analyse en trois catégories distinctes : poésie, chroniques et histoire pour enfant. Chaque sous analyse nous amène à poser une hypothèse différente basée sur le genre littéraire choisi tout en gardant en tête notre hypothèse de départ. C'est au travers de ces analyses que nous tirons notre conclusion finale.

[Poèmes](Laboratoire%20des%20synonymes%200bfb3e6ad9fa4962a56b531520afef0e/Poe%CC%80mes%206917017cba5d4c76a9306c754eb414bf.md)

[Chroniques](Laboratoire%20des%20synonymes%200bfb3e6ad9fa4962a56b531520afef0e/Chroniques%208658b099b13844a68908eb17fd3c080b.md)

[Histoires pour enfant](Laboratoire%20des%20synonymes%200bfb3e6ad9fa4962a56b531520afef0e/Histoires%20pour%20enfant%2041191d251c764bcc97dae6043d7cb47b.md)

### Données globales

- Taille de fenêtre 3

    [https://www.notion.vip/notion-chart/draw.html?config_documentId=1psLfV3vAoiEqAvpchrOy34zOySZfYfojpenYWxgFMq8&config_sheetName=Taille3%20Cluster5&config_dataRange=A1%3AD6&config_chartType=line&config_theme=lightMode&option_colors=%23AD1A72%2C%23E03E3E%2C%230B6E99](https://www.notion.vip/notion-chart/draw.html?config_documentId=1psLfV3vAoiEqAvpchrOy34zOySZfYfojpenYWxgFMq8&config_sheetName=Taille3%20Cluster5&config_dataRange=A1%3AD6&config_chartType=line&config_theme=lightMode&option_colors=%23AD1A72%2C%23E03E3E%2C%230B6E99)

    Cluster5

- Taille de fenêtre 5

    [https://www.notion.vip/notion-chart/draw.html?config_documentId=18bulc_If4eY6UEBMZsqgStcFFpsC9U_hwLbLl58k4Xs&config_sheetName=Taille5-Clusters5&config_dataRange=A1%3AD6&config_chartType=line&config_theme=lightMode&option_colors=%23AD1A72%2C%23E03E3E%2C%230B6E99](https://www.notion.vip/notion-chart/draw.html?config_documentId=18bulc_If4eY6UEBMZsqgStcFFpsC9U_hwLbLl58k4Xs&config_sheetName=Taille5-Clusters5&config_dataRange=A1%3AD6&config_chartType=line&config_theme=lightMode&option_colors=%23AD1A72%2C%23E03E3E%2C%230B6E99)

    **Cluster5**

    [https://www.notion.vip/notion-chart/draw.html?config_documentId=1r_kE8fKWPJuDQ7Ceqf_UW7kM4Hd2Wm1GzNxqnAPucK4&config_sheetName=Taille5%20Cluster15&config_dataRange=A1%3AD16&config_chartType=line&config_theme=lightMode&option_colors=%23AD1A72%2C%23E03E3E%2C%230B6E99](https://www.notion.vip/notion-chart/draw.html?config_documentId=1r_kE8fKWPJuDQ7Ceqf_UW7kM4Hd2Wm1GzNxqnAPucK4&config_sheetName=Taille5%20Cluster15&config_dataRange=A1%3AD16&config_chartType=line&config_theme=lightMode&option_colors=%23AD1A72%2C%23E03E3E%2C%230B6E99)

    **Cluster15**

- Taille de fenêtre 7

    [https://notion.vip/notion-chart/draw.html?config_documentId=1zULvzj9sItcF2jSc3cqOESDZ4ms5aHRJxpHy5WydbpM&config_sheetName=Taille7%20Cluster5&config_dataRange=A1%3AD6&config_chartType=line&config_theme=lightMode&option_colors=%23AD1A72%2C%23E03E3E%2C%230B6E99](https://notion.vip/notion-chart/draw.html?config_documentId=1zULvzj9sItcF2jSc3cqOESDZ4ms5aHRJxpHy5WydbpM&config_sheetName=Taille7%20Cluster5&config_dataRange=A1%3AD6&config_chartType=line&config_theme=lightMode&option_colors=%23AD1A72%2C%23E03E3E%2C%230B6E99)

    **Cluster5**

    [https://www.notion.vip/notion-chart/draw.html?config_documentId=1plxVHJYoHdUFZIKTwkIM_PqXfc4Y5DvhWiMcamF6R4c&config_sheetName=Taille7%20Cluster40&config_dataRange=A1%3AD41&config_chartType=line&config_theme=lightMode&option_colors=%23AD1A72%2C%23E03E3E%2C%230B6E99](https://www.notion.vip/notion-chart/draw.html?config_documentId=1plxVHJYoHdUFZIKTwkIM_PqXfc4Y5DvhWiMcamF6R4c&config_sheetName=Taille7%20Cluster40&config_dataRange=A1%3AD41&config_chartType=line&config_theme=lightMode&option_colors=%23AD1A72%2C%23E03E3E%2C%230B6E99)

    **Cluster40**

Nos analyses portent à croire que les regroupements on en fait un point d'ancrage qui mène à former des grappes et que celles-ci n'ont pas nécessairement une explication grammaticale expliquant leurs liaisons. Il est vrai que le positionnement d'un mot dans une phrase, les cooccurrences qui l'entourent, influence grandement les données, mais plus il y a de regroupement lors de l'analyse et moins ceux-ci sont simple à expliquer.

Pour démontrer cela, à 300 grappes, il est possible d'obtenir un regroupement tel que celui-ci en position 294.

```css
Cluster 294
coeli ----> 5.111111111111112     du latin "vouloir dire"
omnes ----> 6.444444444444445     à l'égard de tous
domino ----> 11.1111111111111     masque, cagoule...
```

Ces mots sont en provenance du même poème qui ont comme point d'encrage "**Benedicite**".

> Benedicite stellae coeli [...]
Benedicite ros domino [...]
Benedicite omnes spirites de [...]

Ainsi, "**Benedicite**" ne peut être dans le regroupement, mais il en est l'influence et la raison même de celui-ci.

Le cas présenté plus haut est toute fois qu'un idéal. Le fait de revoir plusieurs fois un mot dans un texte puis le revoir dans d'autres textes, altère le point d'encrage, sans toute fois changer la logique derrière celui-ci. C'est la distance entre ce point et celui qu'on voit afficher qui nous permets d'avoir des regroupements duquel on peut tirer des conclusions.

## Conclusion

Notre hypothèse globale est infirmée. Il est vrai que plus il y a de grappes, plus les regroupements de mots sont précis, mais ces regroupements n'ont pas nécessairement un lien grammatical entre eux. Ils sont en fait issue d'une popularité initiale à partir d'un mot qui sert de point d'ancrage. Au travers de nos nombreuses analyses, nous avons pu constater que les différence selon le genre littéraire étudié est tel un casse-tête; le positionnement de chaque mot compte. Par contre, le genre n'influence pas la raison d'être des grappes et le lien qui uni leur contenu.