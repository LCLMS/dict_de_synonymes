#!/usr/bin/env python

from Clustering import Clustering
from sys import argv, exit
from traceback import print_exc
from time import time
from entrainementBD import EntrainementBD
from recherche import Recherche
from options import options
from dao import Dao


INVITE = '''
Entrez un mot, le nombre de synonymes que vous voulez et la méthode de calcul,
i.e. produit scalaire: 0, least-squares: 1, city-block: 2

Tapez q pour quitter.

'''
QUITTER = 'q'

def demander_mots(r):
    infos = input(INVITE)
    while infos != QUITTER:
        infos = infos.split()
        mot, nb, methode = infos[0], int(infos[1]), int(infos[2])
        print()
        t = time()
        r.chercher(mot, nb, methode)
        print('\nRecherche en {} secondes'.format(time() - t))
        infos = input(INVITE)

def main():
    try:
        opts = options(argv[1:])

        if opts.databases_path is None:
            raise Exception("Vous devez préciser la BD que vous voulez utiliser (1: poemes, 2: chroniques, 3: histoires pour enfants)")

        dao = Dao(opts.databases_path)
        dao.connecter()

        if opts.bd:
            dao.creer_bd()
        else:
            e = EntrainementBD(opts.fenetre, dao)
            e.chargerBD()
            if opts.entrainement:
                t = time()
                e.entrainer(opts.chemin, opts.encodage)
                print('Entrainement en {} secondes'.format(time() - t))
            elif opts.recherche:
                d, m = e.dico_matrice()
                r = Recherche(d, m)
                demander_mots(r)
            elif opts.clustering:
                dico, matrice= e.dico_matrice()
                tfen, nbResultatsClustering, nbCentroides = opts.fenetre, opts.nombre_resultats_cluster, opts.nombre_centroides
                c = Clustering(nbResultatsClustering, nbCentroides, dico, matrice)
                c.clustering()

        dao.deconnecter()
    except Exception as e:
        print_exc()
        return 1
    return 0


if __name__ == '__main__':
    exit(main())
